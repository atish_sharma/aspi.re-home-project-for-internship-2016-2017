from flask import Flask, request
from flask.ext.mongoalchemy import MongoAlchemy
import time, datetime

PORT = '2006'
APP = Flask(__name__, static_folder = '../app')
APP.config['MONGOALCHEMY_DATABASE'] = 'aspire_home_project'
DB = MongoAlchemy(APP)

class User(DB.Document) :
	google_id = DB.StringField()
	text_data = DB.StringField()
	text_time = DB.StringField()


def getTime() :
	t = int(time.time())
	return datetime.datetime.fromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')

@APP.route('/')
def index() : 
    return APP.send_static_file('index.html')

@APP.route('/<path:path>')
def static_proxy(path):
  return APP.send_static_file(path)

@APP.route('/google_login', methods = ['GET', 'POST'])
def googleLogin() :
	content = request.get_json()
	print(content)
	googleId = content['google_id']
	user = User.query.filter(User.google_id == googleId).first()
	if user == None :
		user = User(google_id = googleId, text_data = '', text_time = '')
		user.save()
	return '{"success" : "true"}'

@APP.route('/get_text', methods = ['GET', 'POST'])
def getText() :
	content = request.get_json()
	googleId = content['google_id']
	user = User.query.filter(User.google_id == googleId).first()
	if user == None :
		return '{"success" : "false"}'	
	return '{"success" : "true", "text_data" : "' + user.text_data + '", "text_time" : "' + user.text_time + '"}'

@APP.route('/set_text', methods = ['GET', 'POST'])
def setText() :
	content = request.get_json()
	googleId, textData, textTime = content['google_id'], content['text_data'], str(getTime())
	user = User.query.filter(User.google_id == googleId).first()
	if user == None :
		return '{"success" : "false"}'
	else :
		user.text_data, user.text_time = textData, textTime
		user.save()
	return '{"success" : "true", "text_data" : "' + user.text_data + '", "text_time" : "' + user.text_time + '"}'

if __name__ == '__main__' :
    APP.run(host = '0.0.0.0', port = PORT)

