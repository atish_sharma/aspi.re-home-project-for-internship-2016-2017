APP.controller('AccountController', function($scope,$route, $routeParams,$location) {

	$scope.TAG = 'AccountController';
	$scope.user = USER;
	$scope.data = {
		text : '',
		time : ''
	};

	$scope.init = function() {
		LogModule.log($scope.TAG, 'init');
		$scope.getText();
	};

	$scope.fin = function() {
		LogModule.log($scope.TAG, 'fin');
	};

	$scope.$on('$routeChangeStart', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeStart');
       	$scope.fin();
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeSuccess');
		$scope.init();
	});

	$scope.logout = function() {	
		LogModule.log($scope.TAG, 'logout');
		USER.logout();
		$scope.switchView('/login');
	};

	$scope.sendText = function() {
		LogModule.log($scope.TAG, 'sendText');
		var res = {
			'google_id' : USER.googleId,
			'text_data' : $scope.data.text
		}
		LogModule.log($scope.TAG, 'res', res);
		$.ajax({
			url : '/set_text',
			data : JSON.stringify(res),
			contentType : 'application/json',
			type : 'POST',
			success : $scope.getTextResponse
		});
	};

	$scope.getText = function() {
		LogModule.log($scope.TAG, 'getText');
		var res = {
			'google_id' : USER.googleId
		}
		LogModule.log($scope.TAG, 'res', res);
		$.ajax({
			url : '/get_text',
			data : JSON.stringify(res),
			contentType : 'application/json',
			type : 'POST',
			success : $scope.getTextResponse
		});
	};

	$scope.getTextResponse = function(data) {
		LogModule.log($scope.TAG, 'getTextResponse');
		data = JSON.parse(data);
		LogModule.log($scope.TAG, 'data', data);
		if(data['success']) {
			$scope.data.text = data['text_data'];
			var t = data['text_time'];
			if(t == '') {
				$scope.data.time = '';
			} else {
				$scope.data.time = 'Text modified @ ' + t;
			}
			$scope.$apply();
		} else {
			window.alert('Unable to fetch data! Please try again.');
		}
	};

	$scope.switchView = function(view) {	
		LogModule.log($scope.TAG, 'switchView');
		$location.path(view);
		$scope.$apply();
	};

});