APP.config(function($routeProvider) {
	$routeProvider
    .when('/', {
    	templateUrl : 'html/login.html',
        controller : 'LoginController'
    })
    .when('/login', {
    	templateUrl : 'html/login.html',
        controller : 'LoginController'
    })
    .when('/account', {
    	templateUrl : 'html/account.html',
        controller : 'AccountController'
    })
    .otherwise({
    	redirectTo:'/'
    });
});