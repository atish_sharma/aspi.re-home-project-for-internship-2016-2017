APP.controller('LoginController', function($scope,$route, $routeParams,$location) {

	$scope.TAG = 'LoginController';

	$scope.init = function() {
		LogModule.log($scope.TAG, 'init');
	};

	$scope.fin = function() {
		LogModule.log($scope.TAG, 'fin');
	};

	$scope.$on('$routeChangeStart', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeStart');
       	$scope.fin();
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeSuccess');
		$scope.init();
	});

	$scope.googleLogin = function() {	
		LogModule.log($scope.TAG, 'googleLogin');
		auth2.signIn().then(function(data) {
			LogModule.log($scope.TAG, 'data', data);
			USER.login(data['w3']['Eea'], data['w3']['ig'], data['w3']['U3']);
			var res = {
				'google_id' : USER.googleId	
			};
			LogModule.log($scope.TAG, 'res', res);
			$.ajax({
				url : '/google_login',
				data : JSON.stringify(res),
				contentType : 'application/json',
				type : 'POST',
				success : $scope.googleLoginResponse 
			});
		});
	};

	$scope.googleLoginResponse = function(data) {       	
		LogModule.log($scope.TAG, 'googleLoginResponse');
		data = JSON.parse(data);
		LogModule.log($scope.TAG, 'data', data);
		if(data['success']) {
			window.alert('Welcome ' + USER.name);
			$scope.switchView('/account'); 
		} else {
			USER.logout();
			window.alert('Unable to login! Please try again.');
		}
	};

	$scope.switchView = function(view) {	
		LogModule.log($scope.TAG, 'switchView');
		$location.path(view);
		$scope.$apply();
	};

});