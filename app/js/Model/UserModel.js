var UserModel = function() {

	var self = this;
	self.TAG = 'UserModel';
	LogModule.logConstructor(self.TAG);
	self.googleId = null;
	self.name = null;
	self.email = null;

	self.clear = function() {
		LogModule.log(self.TAG, 'clear');
		self.googleId = null;
		self.name = null;
		self.email = null;
	};

	self.login = function(googleId, name, email) {
		LogModule.log(self.TAG, 'login');
		self.googleId = googleId;
		self.name = name;
		self.email = email;
	};

	self.logout = function(userId, sessionId, googleId, name, email, picture) {
		LogModule.log(self.TAG, 'logout');
		self.clear();
	};

	self.isLoggedIn = function() {
		LogModule.log(self.TAG, 'isLoggedIn');
		return !_.isNull(self.googleId) && !_.isUndefined(self.googleId) && self.googleId != '';
	};

};

var USER = new UserModel();