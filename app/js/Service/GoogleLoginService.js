var GoogleLoginService = {

	TAG : 'GoogleLoginService',
	GOOGLE_CLIENT_ID : '1027538382485-jfr363m0pk4frc7plv3qi9smdjrfb33g.apps.googleusercontent.com',

	init : function() {
		LogModule.log(this.TAG, 'init');
		var self = this;
		gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
          client_id: self.GOOGLE_CLIENT_ID,
        });
      });
	}

};

LogModule.logConstructor(GoogleLoginService.TAG);